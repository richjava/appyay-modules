import utilStyles from '../../styles/utils.module.css'
import introStyles from './intro.module.css'

export default function Intro() {
  return (
    <section className={utilStyles.headingMd, introStyles.intro}>
        <p>[Your Self Introduction]</p>
        <p>
          (This is a sample website - you’ll be building a site like this in{' '}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>
  )
}
