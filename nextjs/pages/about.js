import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'

export default function About({ allPostsData }) {
  return (
    <Layout>
      <Head>
        <title>About</title>
      </Head>
      <p>This is the about page</p>
      
    </Layout>
  )
}
